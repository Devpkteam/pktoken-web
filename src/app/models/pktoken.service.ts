
import { pkToken } from '../models/pktoken.model';
import { pkTokenn } from '../models/pktokenn.model';
import { Injectable } from '@angular/core';
import { Usuario } from 'src/app/models/usuario.model';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from 'src/app/config/config';
import { map } from 'rxjs/operators'
import { catchError } from 'rxjs/operators'
import swal from 'sweetalert';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class PktokenService {
  constructor(
    public http: HttpClient,
    public router: Router,
  ) { }
  
  getpktokens() {
    let url = URL_SERVICIOS + '/pktoken/';
    console.log(url);
      return this.http.get( url )
        .pipe(map( (resp: any) =>{
          return resp.usuarios;
        }),catchError( err => {
          swal( 'No se pudo generar el pkToken', 'el pkToken generado ya existia' ,'error' );
          location.reload();
          return throwError(err)
        }));
  }

  getToken(id: string) {
    let url = URL_SERVICIOS + '/pktoken/' + id;
    console.log(url);
      return this.http.get( url )
      .pipe(map( (resp: any) =>{
      }),catchError( err => {
        swal( 'No se pudo generar el pkToken', 'el pkToken generado ya existia' ,'error' );
        //location.reload();
        return throwError(err)
      }));
  }

  crearPkToken( pkToken: pkToken ) {
    let url = URL_SERVICIOS +'/pktoken';    
    return this.http.post(url, pkToken)
    .pipe(map( (resp:any) => {
        console.log(resp);
        swal('pkToken creado: ', resp.pktokenGuardado._id, 'success');     
        return resp.pktokenGuardado._id;
    }),
    catchError( err => {
      console.log( err ) 
      swal('Error' ,`${err}` ,'error' )
      return throwError(err)
    }));
  }
}
