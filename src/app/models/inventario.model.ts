export class Inventario {
    constructor (
        public nombre: String,
        public cantidad: Number, 
        public costo: Number,
        public _id?: String,
    ) {}
}