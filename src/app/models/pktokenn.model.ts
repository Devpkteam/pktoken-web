import { duenio } from "./duenio.model";
export class pkTokenn {

    constructor (
        public _id: any,
        public monto: String,
        public fechaCreado: Date,
        public duenioActual: duenio,
        public estatus: Boolean,
        public __v: Number,
    ) { }

}