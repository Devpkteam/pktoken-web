import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PktokenService } from '../../services/pktoken/pktoken.service';
import { pkToken } from '../../models/pktoken.model';
import { Usuario } from '../../models/usuario.model';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


import {DomSanitizer} from '@angular/platform-browser';
import { setupRouter } from '@angular/router/src/router_module';
//import * as html2canvas from 'html2canvas';
//import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-generator',
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.css']
})
export class GeneratorComponent implements OnInit {

  img;
  img2;
  public descargar: boolean = false;
  value;
  public idToken : any;
  public idlote : any;
  numeroLote;
  loteGenerado = false;
  monto;
  public open: boolean = false;
  linkpdf;
  public lote: number ;
  public serialId: string;
  public bandera: boolean = false;
  public nlote: boolean = false;
  constructor(
    public _pkTokenService: PktokenService,
    private sanitizer:DomSanitizer
  ) { }

  ngOnInit() {
    console.log(this.numeroLote)

  }

  check() {
    this.value = parseInt(this.numeroLote);
    console.log(this.value)
    this.loteGenerado =true;
  }

  download() {

    let idSerial;
    let SerialLote;
    this.open= true;
    console.log(this.open)
    this.idlote =  JSON.stringify({idlote:this.tokenLote()});
    SerialLote = (JSON.parse(this.idlote).idlote);

    for ( var i=1; i <=this.value ; i++ ) {
      this.idToken = JSON.stringify({_id:this.tokenNumber().substring(10,5), monto:this.monto});
      this.serialId = '10 ' + '10 ' + SerialLote.substring(10,5) + " " +  JSON.parse(this.idToken)._id + " 20"
    }
    let LOTE = JSON.parse(this.idlote);

    this._pkTokenService.crearPkTokens(this.numeroLote, this.monto, this.serialId).subscribe((pdf)=>{
      //console.log(pdf);
     /* saveAs(file, 'report.pdf');
      let sanitizedUrl = this.sanitizer.bypassSecurityTrustUrl(pdf);
      window.open(pdf);
      console.log(sanitizedUrl)
      this.linkpdf = pdf;*/
      this.open=false;
    });
  }

  sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustUrl(url);
}

  tokenNumber() {
    return 'xxxxxxxxxxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
  tokenLote() {
    return 'xxxxxxxxxxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

}
