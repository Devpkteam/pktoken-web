import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert'; 
import { UsuarioService } from '../../services/service.index';
import { Usuario } from '../../models/usuario.model';
import { Router } from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";


declare function init_plugins();

@Component({
  selector: 'app-modaledit',
  templateUrl: './modaledit.component.html',
  styleUrls: ['./modaledit.component.css']
})
export class ModaleditComponent implements OnInit {
  user;

  form: FormGroup;

  constructor(
    public _usuarioService: UsuarioService,
    public router: Router,
    private dialogRef: MatDialogRef<ModaleditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: object
  ) { }


  ngOnInit() {
    init_plugins();
    this.form = new FormGroup({
      nombre: new FormControl( null , Validators.required),
      email: new FormControl( null , [Validators.required, Validators.email] ),
    });
    console.log(this.data);
  }

  modificar(usuario:Usuario){
    console.log(this.form.valid);
    console.log(this.form.value);
    if ( this.form.invalid ) {
      return;
    }

    usuario.nombre = this.form.value.nombre;
    usuario.email = this.form.value.email;
    

    console.log(usuario);

    this._usuarioService.actualizarUsuario( usuario )
      .subscribe( resp => {console.log(resp);this.dialogRef.close();})
  }
  
  close() {
    this.dialogRef.close();
  }
}
