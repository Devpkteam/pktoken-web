import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert';
import { InventarioService } from '../../services/service.index';
import { Inventario } from '../../models/inventario.model';;
import { Router } from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";


declare function init_plugins();

@Component({
  selector: 'app-modal2',
  templateUrl: './modal2.component.html',
  styleUrls: ['./modal2.component.css']
})
export class ModaleditComponent2 implements OnInit {
  user;

  form: FormGroup;

  constructor(
    public _inventarioService: InventarioService,
    public router: Router,
    private dialogRef: MatDialogRef<ModaleditComponent2>,
    @Inject(MAT_DIALOG_DATA) public data: object
  ) { }


  ngOnInit() {
    init_plugins();
    this.form = new FormGroup({
      nombre: new FormControl( null , Validators.required),
      cantidad: new FormControl( null ,Validators.required ),
      costo: new FormControl( null ,Validators.required ),
    });
    console.log(this.data);
    console.log(this.form.value);
    console.log(this.form)
  }

  modificar(inventario: Inventario){
   console.log(inventario);
   // this.form.value.costo = inventario.costo;
   // this.form.value.cantidad = inventario.cantidad;
   // this.form.value.nombre = inventario.nombre;
   
   // console.log(this.form.valid);
    // console.log(this.form.value);
    // if ( this.form.invalid ) {
      //   return;
      // }
      // console.log(this.form.value)
      //   inventario.nombre = this.form.value.nombre;
      //   inventario.cantidad = this.form.value.cantidad;
      //   inventario.costo = this.form.value.costo;
      //   console.log(inventario);
      if (this.form.invalid ) {
          return;
          console.log('entro')
        }

      this.form.value._id = inventario;
      console.log(this.form.value);
      
      this._inventarioService.actualizarInventario( this.form.value, localStorage.getItem('token') )
      .subscribe( resp => {
        console.log(resp);
        this.dialogRef.close();
      })
  }
  
  close() {
    this.dialogRef.close();
  }
}