import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaleditComponent2 } from './modal2.component';

describe('ModaleditComponent2', () => {
  let component: ModaleditComponent2;
  let fixture: ComponentFixture<ModaleditComponent2>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModaleditComponent2 ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModaleditComponent2);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
