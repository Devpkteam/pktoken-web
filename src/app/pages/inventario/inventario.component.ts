import { Component, OnInit } from '@angular/core';
import { InventarioService } from '../../services/service.index';
import { Inventario } from '../../models/inventario.model';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {MatDialog, MatDialogConfig} from "@angular/material";
import { ModaleditComponent2 } from '../modal2/modal2.component';

declare var swal: any;
@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styles: ['./inventario.component.css']
})

export class InventarioComponent implements OnInit {
  
  inventario: Inventario[] = [];
  desde: number = 0;
  cargando: boolean = true;
  totalRegistros: number = 0;
  form: FormGroup;
  opcion = false;
  cont = 0;
  isChecked: false;
  
  constructor(
    public _inventarioService: InventarioService,
    public router:Router,
    private dialog: MatDialog
  ) { }

  

  ngOnInit() {
    this.form = new FormGroup({
      nombre: new FormControl( null , Validators.required),
      cantidad: new FormControl( null , Validators.required ),  
      costo: new FormControl( null , Validators.required ),
    });
    this.cargarInventario();
  }
  onChange(e){
    console.log(e)
    //this.opcion= true;
    this.isChecked = e;
    console.log(this.isChecked)
    
  }

  incrementar(){
    this.cont=this.cont + 1;
    console.log(this.cont)
  }

  registrarInventario(){
    console.log(this.form.valid);
    console.log(this.form.value);
    if ( this.form.invalid ) {
      return;
    }

    let inventario = new Inventario(
      this.form.value.nombre,
      this.form.value.cantidad,
      this.form.value.costo
    );
    console.log(inventario);
    this._inventarioService.crearInventario( inventario )
      .subscribe( resp => console.log(resp))
  }
  
  cargarInventario() {
  //  console.log(this.inventario);
    this.cargando = true;
    this._inventarioService.cargarInventario( this.desde )
    .subscribe( (resp: any) => {
      this.ngOnInit();
      this.totalRegistros = resp.total;
      this.inventario = resp.inventario;
      this.cargando = false;
    //  console.log(this.inventario);

    })
  }

  Actualizar( inventario: Inventario ) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.data = {
      inventario: inventario
    }
  
   this.dialog.open(ModaleditComponent2, dialogConfig);
  }
  borrarProducto( inventario: Inventario ) {
    swal({
      title: "¿Estas seguro?",
      text: 'Esta apunto de borrar el ' + inventario.nombre,
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then( borrar => {
      if ( borrar ) {
        let idUser: any = inventario._id;
        this._inventarioService.borrarProducto( idUser )
          .subscribe( ( borrado: boolean ) => {
            console.log(borrado);
            this.cargarInventario();
          })
      } 
    });
  }
}

