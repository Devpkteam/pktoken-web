import { Component, OnInit } from '@angular/core';
import { PkrecordService } from 'src/app/services/pkrecords/pkrecord.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.css']
})
export class RecordComponent implements OnInit {
  displayedColumns: string[] = ['id', 'fecha', 'email_origen','usuario_origen','email_destino', 'usuario_destino'];

  record: any[];

  constructor(public pkrecord:PkrecordService, public router:Router) {
    let id = localStorage.getItem('idPkoinRecord')
    this.pkrecord.getRecordByIdPkToken(id).subscribe((data:any)=>{
      console.log(data)
      this.record = data.pkrecordsbyid
    })
   }

  ngOnInit() {
    console.log(this.record)
  }

  back(){
    this.router.navigate(['/dashboard'])
  }

}
