import { Component, OnInit } from '@angular/core';
import { pkTokenn } from '../../models/pktokenn.model';
import { PktokenService } from '../../services/pktoken/pktoken.service';
import { PkrecordService } from 'src/app/services/pkrecords/pkrecord.service';
import { Router } from '@angular/router';


export interface PeriodicElement {
  id: string;
  nombre: number;
  monto: number;
  date: string;
}
declare var swal: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: ['./dashboard.component.css']
})



export class DashboardComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nombre', 'monto','date','record', 'actions'];

  pkTokens: pkTokenn[];

  constructor(
    public _pkTokenService: PktokenService,
    public pkrecord:PkrecordService,
    public router:Router
  ) { }

  getPktokens() {
    this._pkTokenService
    .getPktokens()
    .subscribe((data: pkTokenn[]) => {
    this.pkTokens = data;
    console.log(this.pkTokens);
    })
    }

  ngOnInit() {
    this.getPktokens();
    
  }

  borrarPk(pktoken:pkTokenn){

    swal({
      title: "¿Estas seguro?",
      text: 'Esta apunto de borrar al pktoken ID: ' + pktoken._id,
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then( borrar => {
      if ( borrar ) {
        let id: any = pktoken._id;
        this._pkTokenService.borrarPktoken( id )
          .subscribe( ( borrado: boolean ) => {
            console.log(borrado);
            this.getPktokens();
          })
      } 
    });
  }

  historialPk(id){
    localStorage.setItem('idPkoinRecord',id)
    this.router.navigate(['/recordpk'])
  }
  

  

}

