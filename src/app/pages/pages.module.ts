import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Graficas1Component } from './graficas1/graficas1.component';
import { SharedModule } from '../shared/shared.module';
import { PAGES_ROUTES } from './pages.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IncrementadorComponent } from '../components/incrementador/incrementador.component';
import { ChartsModule } from 'ng2-charts';
import { GraficaDonaComponent } from '../components/grafica-dona/grafica-dona.component';
import { CommonModule } from '@angular/common';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { PipesModule } from '../pipes/pipes.module';
import { ProfileComponent } from './profile/profile.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { HospitalesComponent } from './hospitales/hospitales.component';
import { MedicosComponent } from './medicos/medicos.component';
import { MedicoComponent } from './medicos/medico.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { GeneratorComponent } from './generator/generator.component';
import { ModaleditComponent } from './modaledit/modaledit.component';
import { InventarioComponent } from './inventario/inventario.component';
import { ModaleditComponent2 } from './modal2/modal2.component';

/* ANGULAR MATERIALS*/
import {MatTableModule} from '@angular/material/table';
import {MatDialogModule, MatIconModule} from "@angular/material";
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

/* ENTRY COMPONENTS */
import  { RegisterComponent } from '../login/register.component';

/* FORMS */

// Servicios
import { ServiceModule } from '../services/service.module';



import { NgxQRCodeModule } from 'ngx-qrcode2';
import { RecordComponent } from './record/record.component';


@NgModule ({
    declarations: [
        DashboardComponent,
        ProgressComponent,
        Graficas1Component,
        IncrementadorComponent,
        GraficaDonaComponent,
        AccountSettingsComponent,
        PromesasComponent,
        RxjsComponent,
        ProfileComponent,
        UsuariosComponent,
        HospitalesComponent,
        MedicosComponent,
        MedicoComponent,
        BusquedaComponent,
        GeneratorComponent,
        RegisterComponent,
        ModaleditComponent,
        InventarioComponent,
        ModaleditComponent2,
        RecordComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        PAGES_ROUTES,
        FormsModule,
        ChartsModule,
        CommonModule,
        PipesModule,
        NgxQRCodeModule,
        MatTableModule,
        MatDialogModule,
        MatDialogModule, 
        MatInputModule, 
        MatButtonModule, 
        MatCardModule, 
        MatFormFieldModule,
        MatSlideToggleModule,
        ReactiveFormsModule,
        ServiceModule,
        MatIconModule
    ],
    exports: [
        DashboardComponent,
        ProgressComponent,
        Graficas1Component,
        GraficaDonaComponent
    ],
    entryComponents: [RegisterComponent, ModaleditComponent, ModaleditComponent2]
})
export class PagesModule {}
