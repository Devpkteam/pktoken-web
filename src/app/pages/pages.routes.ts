import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Graficas1Component } from './graficas1/graficas1.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { LoginGuardGuard, AdminGuard, VerificatokenGuard } from '../services/service.index';
import { ProfileComponent } from './profile/profile.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { HospitalesComponent } from './hospitales/hospitales.component';
import { MedicoComponent } from './medicos/medico.component';
import { MedicosComponent } from './medicos/medicos.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { GeneratorComponent } from './generator/generator.component';
import { RecordComponent } from './record/record.component';
import { InventarioComponent } from './inventario/inventario.component';

const pagesRoutes: Routes = [
    { 
        path: 'dashboard', 
        component: DashboardComponent,
        canActivate: [ VerificatokenGuard ],
        data: { titulo: 'dashboard'} 
    },
    
    { path: 'billetes', component: DashboardComponent, data: { titulo: 'Billetes generados'}  },
    { path: 'generador', component: GeneratorComponent, data: { titulo: 'Generador'}  },
    { path: 'inventario', component: InventarioComponent, data: { titulo: 'Inventario del PKafe'} } ,
    //mantenimiento
    { 
        path: 'usuarios', 
        component: UsuariosComponent,
        
        data: { titulo: 'Mantenimiento de usuario'}  
    },

    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: 'recordpk', component:RecordComponent, data: {titulo:'Historial'}}
];

export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );
