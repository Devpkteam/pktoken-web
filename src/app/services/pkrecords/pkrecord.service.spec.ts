/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PkrecordService } from './pkrecord.service';

describe('Service: Pkrecord', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PkrecordService]
    });
  });

  it('should ...', inject([PkrecordService], (service: PkrecordService) => {
    expect(service).toBeTruthy();
  }));
});
