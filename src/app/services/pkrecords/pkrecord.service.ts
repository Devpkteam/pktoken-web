import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from 'src/app/config/config';

@Injectable({
  providedIn: 'root'
})
export class PkrecordService {

constructor(public http:HttpClient) { }


getRecordByIdPkToken(id){
  return this.http.get(`${URL_SERVICIOS}/pkhistory/${id}`)
}
}
