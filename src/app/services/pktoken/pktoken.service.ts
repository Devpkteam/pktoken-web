
import { pkToken } from '../../models/pktoken.model';
import { Injectable } from '@angular/core';
import { Usuario } from 'src/app/models/usuario.model';
import { RequestOptions, ResponseContentType } from '@angular/http';
import {HttpHeaders} from "@angular/common/http";
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from 'src/app/config/config';
import { map } from 'rxjs/operators'
import { catchError } from 'rxjs/operators'
import swal from 'sweetalert';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';
import { saveAs } from 'file-saver';
@Injectable({
  providedIn: 'root'
})
export class PktokenService {
  constructor(
    public http: HttpClient,
    public router: Router,
  ) { }

  //obtener pktokens

  getPktokens(){
    let url = URL_SERVICIOS + '/pktoken/';
    console.log(url);
      return this.http.get( url )
      .pipe(map( (resp: any) =>{
        return resp.usuarios;
      }),catchError( err => {
        swal( 'No se consiguio los pkTokens' );
        location.reload();
        return throwError(err)
      }));
  }


  //obtener un pktoken


  getToken(id: string) {
    let url = URL_SERVICIOS + '/pktoken/' + id;
    console.log(url);
      return this.http.get( url )
      .pipe(map( (resp: any) =>{
      }),catchError( err => {
        swal( 'No se pudo generar el pkToken', 'el pkToken generado ya existia' ,'error' );
        location.reload();
        return throwError(err)
      }));
  }


  //crear pktokens
  crearPkTokens(cantidad, monto, lote){


    let url = URL_SERVICIOS +'/pktoken/'+cantidad+'/'+monto+'/'+ lote +'';
    return this.http.get(url, {responseType: 'arraybuffer' as 'json'})
    .pipe(map(
      data => {
         var file = new Blob([data], {type: 'application/pdf'});
         //var fileURL = URL.createObjectURL(file);
         //saveAs(blob, 'report.pdf');
         saveAs(file, 'Pkoins_lote-'+lote+'.pdf');

      })
      );
    }



  crearPkToken( pkToken: pkToken ) {
    let url = URL_SERVICIOS +'/pktoken';
    return this.http.post(url, pkToken)
    .pipe(map( (resp:any) => {
        console.log(resp);
        swal('pkToken creado: ', resp.pktokenGuardado._id, 'success');
        return resp.pktokenGuardado._id;
    }),
    catchError( err => {
      console.log( err )
      // swal('Error' ,`${err}` ,'error' )
      return throwError(err)
    }));
  }

  //eliminar pktokens

  borrarPktoken(id : string){
    let url = URL_SERVICIOS + '/pktoken/' + id;
    return this.http.delete( url )
      .pipe(map( resp => {
        swal("Pktoken borrado","el pktoken ha sido eliminado correctamente","success");
        return true;
      }),catchError( err => {
        swal( 'No se consiguio el pkToken' );
        location.reload();
        return throwError(err)
      }));
  }



}
